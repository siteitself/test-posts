# posts

## Project setup
```
cd test-posts/posts
npm install
```
## install and run json server
```
npm i -g json-server
cd test-posts
json-server db.json
```
### Compiles and hot-reloads for development
```
npm run serve
```
### 
### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
