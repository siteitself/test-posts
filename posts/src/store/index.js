import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [],
  },
  getters: {
    getPosts(state) {
      return state.posts;
    },
  },
  mutations: {
    setPosts(state, payload) {
      state.posts = payload;
    },
    updateLike(state, post) {
      const postId = state.posts.findIndex((el) => el.id === post.id);
      state.posts.splice(postId, 1, post);
    },
  },
  actions: {
    async getAllPosts({ commit }) {
      await axios.get('http://localhost:3000/posts')
        .then((response) => {
          commit('setPosts', response.data);
        });
    },
    async sendLike({ commit }, post) {
      await axios.patch(`http://localhost:3000/posts/${post.id}`, post)
        .then((res) => {
          commit('updateLike', res.data);
        });
    },
  },
  modules: {
  },
});
